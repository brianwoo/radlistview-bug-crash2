import { Item, Data } from './../../shared-types/dataItem';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy, DoCheck, ViewContainerRef } from '@angular/core';
import { ObservableArray, ChangedData } from 'tns-core-modules/data/observable-array';
import { ListViewEventData, RadListView } from "nativescript-pro-ui/listview"
import { RadListViewComponent } from "nativescript-pro-ui/listview/angular"
import { EventData } from 'tns-core-modules/ui/frame';

@Component({
  moduleId: module.id,
  selector: 'add-new-data',
  templateUrl: './add-new-data.component.html',
  styleUrls: ['./add-new-data.component.scss']
})
export class AddNewDataComponent implements OnInit {

  _data: Data;

  private _dataItems: ObservableArray<Item>;

  constructor(private vcRef: ViewContainerRef) {
  }




  ngOnInit() {

    this.buildDataItems();
  }

  get dataItems(): ObservableArray<Item> {
    return this._dataItems;
  }


  /**
   */
  private buildDataItems() {

    this._data = new Data();
    this._dataItems = new ObservableArray(this._data.itemList);
  }



}

